from collections import deque
class LFSR:

    def __init__(self,formula = [1,0,0,1,0,1,0,0,0]):
        """
        Initialisation de la classe avec une formule
        @param formula, tableau de bit représentant la formule
        """
        self.formula = formula
        self.current_state = []

    def initValues(self,values= [1,1,1,1,1,1,1,1,1]):
        '''
        Défini les valeurs initiales de notre LFSR
        @param values, initial state of the LFSR
        '''
        if len(self.current_state) != len(values) :
            self.current_state = deque(values)

    def generateNextCombinaison(self):
        '''
        Permet de générer la prochaine valeur Bn et met à jour le tableau des états
        @return etat courant du registre LFSR
        '''
        result = 0;
        for i in range(len(self.formula)):
            if(self.formula[i] == 1):
                result = result ^ self.current_state[i]
        self.current_state.appendleft(result);
        self.current_state.pop()
        #print(self.current_state)
        return self.current_state

    def random(self):
        result  = 0
        self.generateNextCombinaison()
        for i in range(len(self.formula)):
            #print(len(self.formula)-i-2)
            result += pow(2,i) * self.current_state[len(self.formula)-i-2]
        result = float(result)/float(pow(2,len(self.formula)))
        return result

    def generate_x_random(self, x):
        '''
        Génère deux array, une correspondant au x et une correspondant au lsb du x-ième random
        @param x, nombre de valeurs à générer
        '''
        array  = []
        array2 = []
        for i in range(x):
            array.append(i)
            k = self.random()
            array2.append(k)
        return array,array2
