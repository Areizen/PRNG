from src.LFSR import LFSR
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

lfsr = LFSR(formula = [0,1,0,1,0,1,0,1,0,1,0,0,0,1,1,1,0,1,0,0,0,0]);
lfsr.initValues(values = [0,1,0,1,0,1,0,1,0,1,0,0,0,1,1,1,0,1,0,0,0,0])

fig,ax = plt.subplots()
x,y = lfsr.generate_x_random(1000)
ax.scatter(x,y)
ax.grid()

plt.show()
