# PRNG Linear Feedback Shift Register

### Installation

```bash
sudo apt install tk
sudo pip3 install -r requirements.txt
```

### Question 1 : périodicité

Nous retomberons forcément sur une valeur car les possibilités de résultats sont finis.

La périodicité maximale d'un LFSR à 30 bits est de 2³⁰-1 soit `1 073 741 823`.

### Question 2 : Calculer à al main les bits générés par bn&nbsp;=&nbsp;bn-4&nbsp;⊕&nbsp;bn-3

Lorsque l'on prend un état de départ ne contenant que des 1, la périodicité finale est de 23

Lorsque l'on prend un état initial ne contenant que des 0, la périodicité est de 0 puisque 0 ⊕ 0 = 0

<center>
  <img width="60%" src="./assets/periodicite_1.jpg">
</center>

### Question 3 :  Écrire une fonction qui travaille sur 8 bits
