import time
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from src.Custom_Random import Custom_Random

c = Custom_Random()

fig,ax = plt.subplots()
x,y = c.generate_x_random(1000000)
ax.scatter(x,y)
ax.grid()

plt.show()
