# PRNG Congruenciel


- [PRNG Congruenciel](#prng-congruenciel)
	- [Installation](#installation)
    - [Question 1 : Programmer un tel générateur aléatoire avec comme fonction&nbsp;f](#question-1-programmer-un-tel-gnrateur-alatoire-avec-comme-fonctionnbspf)
	- [Question 2 : Expliquer pourquoi un tel générateur est toujours périodique.](#question-2-expliquer-pourquoi-un-tel-gnrateur-est-toujours-priodique)
	- [Question 3 : Regardez ce qui se passe sur le bit de poids faible .](#question-3-regardez-ce-qui-se-passe-sur-le-bit-de-poids-faible-)



### Installation
```bash
sudo apt install tk
sudo pip3 install -r requirements.txt
```

### Question 1 : Programmer un tel générateur aléatoire avec comme fonction&nbsp;f

[Voir source](src/Custom_Random.py)

### Question 2 : Expliquer pourquoi un tel générateur est toujours périodique.

Le fait d'utiliser un modulo pour limiter l'opération, rend l'opération périodique, en effet si on retombe sur un nombre ( ce qui est forcément possible vue le nombre de possibilités ).

Le nombre généré reste le même au bout de la 23ème fois.

![alt="Periodicité"](assets/Periodicite.png)

### Question 3 : Regardez ce qui se passe sur le bit de poids faible .

> However, on older rand() implementations, and on current implementations on different systems, the lower-order bits are much less random than the higher-order bits.



En fonction de a,b,zn et m :
- Si a est paire, a*zn sera paire et le résultat sera de la même parité que b
![alt="Résultat 1"](assets/Figure_1.png)

- Si a est impaire, la parité de a*zn sera défini par zn et en fonction de b, mais à l'étape d'aprés, zn+1 aura la parité opposée
![alt="Résultat 2"](assets/Figure_2.png)
