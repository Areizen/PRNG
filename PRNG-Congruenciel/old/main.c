#include <stdio.h>
#include <time.h>
#include <math.h>

void init_rand(int *a,int *b,int *k,int *m);
int custom_rand(int a, int b, int *k, int m);

int main(int argc, char** argv){
  int a,b,k,m;
  init_rand(&a,&b,&k,&m);
  while(1){
    printf("%ld\n",custom_rand(a,b,&k,m));
    sleep(100);
  }
  return 0;
}

/**
Prends une seed en paramètre et renvoi un nombre généré
@param seed
@return int, valeur aléatoire retournée
*/
void init_rand(int *a,int *b,int *k,int *m){
    *m = (int)pow(2,32);
    *a = 1664525;
    *b = 1013904223;
    *k = (int)time(NULL)%(*m);
}

int custom_rand(int a,int b,int *k,int m){
  *k = (a * (*k) + b) % m;
  return *k;
}
