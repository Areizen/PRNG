#!/usr/bin/python2
# -*- coding: utf-8 -*-
from z3 import *
from src.Custom_Random import Custom_Random

'''
Ceci est un programme qui va casser la prochaine génération du générateur congruentiel grâce
à Z3-Solver (https://github.com/Z3Prover/z3)

Usage :
sudo pip2.7 install z3-solver
python2.7 crack.py

/!\ Z3 n'étant pas compatible python3 nous utilisons ici python2
'''

'''
Listes des combinaisons connues utilisées pour le LCG
(https://en.wikipedia.org/wiki/Linear_congruential_generator)
'''
known_combinaisons = [
    ("Numerical Recipes",pow(2,32),1664525,1013904223,-1),
    ("Borland C/C++",pow(2,32),22695477,1,(30,16)),
    ("Borland C/C++",pow(2,32),22695477,1,(30,0)),
    ("glibc (used by GCC)",pow(2,31),1103515245,12345,(30,0)),
    ("ANSI C: Watcom, Digital Mars, CodeWarrior, IBM VisualAge C/C++ [10] C99, C11: Suggestion in the ISO/IEC 9899 [11]",pow(2,31),1103515245,12345,(30,16)),
    ("Borland Delphi, Virtual Pascal",pow(2,32),134775813,1,(63,32)),
    ("Turbo Pascal",pow(2,32),134775813,1,-1),
    ("Microsoft Visual/Quick C/C++",pow(2,32),214013,2531011,(30,16)),
    ("Microsoft Visual Basic (6 and earlier)",pow(2,32),1140671485,12820163,-1),
    ("RtlUniform from Native API",pow(2,31)-1,2147483629,2147483587,-1),
    ("Apple CarbonLib, C++11's rand0",pow(2,31)-1,16807,0,-1),
    ("Apple CarbonLib, C++11's rand",pow(2,31)-1,48271,0,-1),
    ("MMIX by Donald Knuth",pow(2,64),6364136223846793005,1442695040888963407,-1),
    ("Newlib, Musl",pow(2,64),6364136223846793005,1,(63,32)),
    ("VMS's MTH$RANDOM,[15] old versions of glibc",pow(2,32),69069,1,-1),
    ("Java's java.util.Random, POSIX [ln]rand48, glibc [ln]rand48[_r]",pow(2,48),25214903917,11,(47,16)),
    ("POSIX[21] [jm]rand48, glibc [mj]rand48[_r]",pow(2,48),25214903917,11,(47,15)),
    ("POSIX [de]rand48, glibc [de]rand48[_r]",pow(2,48),25214903917,11,(47,0)),
    ("cc65[22]",pow(2,23),65793,4282663,(22,8)),
    ("cc65",pow(2,32),16843009,826366247,(31,16)),
    ("RANDU",pow(2,31),65539,0,-1)
]


def calculateNext(m,a,b,c,bytes = -1):
    next = -1
    if(bytes != -1):
        start,end = bytes
        next  = (c * a + b) % m
        next  =bin(next)[2:]
        next  = "0"*(64-len(next)) + next
        next  = next[len(next)-start-1:len(next)-end-1]
        next  = int(next,2)
    else:
        next  = (c * a + b) % m
    return next

def process():
    '''
    Fonction qui va calculer le nombre d'éléments nécessaires
    pour pouvoir casser à 100% le prochain nombre àléatoire
    '''
    array = []
    c = Custom_Random()
    result = -1
    model = -1
    i = 0
    '''
    On en ajoute un certain nombre au départ afin d'avoir une certaine base
    '''
    print("[+] Génération de 3 nombres aléatoires :\n")
    while(i<3) :
            new = c.generate_random_int()
            print('New random number : '+str(new))
            array.append(new)
            i = i + 1
    print("\n[+] Recherche de la combinaison :")
    if(not try_know_factor(array)):
        print("[-] La combinaison n'est pas une combinaison connue ...")
        print("[+] Attaque par SAT Solver :")
        while(i<20) :
            new = c.generate_random_int()
            print('New random number : '+str(new))
            if(new == result):
                print("Found parameters")
                result = calculateNext(model[0],model[1],model[2],result)
                print("Next number is :"+str(result))
                break
            array.append(new)
            result,model= try_break(array)
            i = i + 1

def try_know_factor(array_number):
    '''
    Fonction qui essayer les facteurs connu et utilisé de générateurs aléatoires
    '''
    if(len(array_number)<2):
        print("Vous devez avoir au moins deux nombres générés aléatoirement !")
        return False
    for i in known_combinaisons :
        label,m,a,b,bytes = i
        if(calculateNext(m,a,b,array_number[0], bytes) == array_number[1]):
            print("\nFound model : [m="+str(m)+",a="+str(a)+",b="+str(b)+"]")
            print("It's the random function from : " + label)
            print("Next number is :"+str(calculateNext(m,a,b,array_number[len(array_number)-1], bytes)))
            return True
    return False


def try_break(array_number):
    '''
    Fonction qui va essayer de casser le prochain élément
    '''
    s = Solver()

    '''
    Les quatres variables que l'on veut retrouver
    '''
    m = Int('m')
    a = Int('a')
    b = Int('b')

    '''
    On ajoute les conditions nécessaires à ce que notre SAT Solver puisse résoudre
    le problème
    '''
    s.add(m>0,a>0,b>0,a<m,b<m)
    for i in range(0,len(array_number)-1):
        s.add(array_number[i + 1] == (( array_number[i] * a + b ) % m) )
    results = -1
    print("Checking :")
    while(sat == s.check()):
        print("ok")
        print("Calculating :")
        model = s.model()
        print(model)
        result = calculateNext(model[m].as_long(),model[a].as_long(),model[b].as_long(),array_number[len(array_number)-1])
        return result,[model[m].as_long(),model[a].as_long(),model[b].as_long(),array_number[len(array_number)-1]]

    print("not ok")
    return -1,-1

if __name__ == '__main__' :
    process()
