# -*- coding: utf-8 -*-
import time

class Custom_Random:

    def __init__(self):

	'''        
        self.m = pow(2,32)
        self.a = 134775813
        self.b = 1
        self.k = int(time.time()) % self.m

        '''
        self.m = 500
        self.a = 15
        self.b = 11
        self.k = int(time.time()) % self.m


    def generate_random_float(self):
        '''
        Génère un nombre k via un algorithme de génération pseudo aléatoire congruenciel
        @return k, entier généré
        '''
        self.k = (self.a * self.k + self.b) % self.m
        return float(self.k)/float(self.m)

    def generate_x_random(self,x):
        '''
        Génère deux array, une correspondant au x et une correspondant au y ( y généré aléatoirement )
        @param x, nombre de valeurs à générer
        '''
        array  = []
        array2 = []
        for i in range(x):
            array.append(i)
            array2.append(self.generate_random_float())
        return array,array2

    def generate_random_int(self):
        '''
        Génère un nombre k via un algorithme de génération pseudo aléatoire congruenciel
        @return k, entier généré
        '''
        self.k = (self.a * self.k + self.b) % self.m
        return self.k

    def generate_x_random_lsb(self, x):
        '''
        Génère deux array, une correspondant au x et une correspondant au lsb du x-ième random
        @param x, nombre de valeurs à générer
        '''
        array  = []
        array2 = []
        for i in range(x):
            array.append(i)
            k = self.generate_random_int()%2
            array2.append(k)
        return array,array2
