# PRNG - Qualité d'un générateur pseudo aléatoire

# Pour chaque partie voir le dossier associé

### Question 1 - Pour les deux générateurs tracer un graphe de 1000 points générés aléatoirement

<center>
 <img src="./assets/Congruential_1000.png">
 <p><i>Figure 2: 1000 points générés avec un PRNG Congruentiel</i></p>
</center>


<center>
 <img src="./assets/LFSR_1000.png">
 <p><i>Figure 2: 1000 points générés avec un PRNG utilisant du LFSR</i></p>
</center>

### Question 2 : Commentez les résultats

Via les graphiques aucun des deux générateurs de nombre aléatoire ne semble meilleur que l'autre.
En effet, pour les deux les points sont plutôt bien répartis sur les graphes.

### Qestion 3 : Modifier les valeurs et commentez
